FROM tomcat:8

MAINTAINER 10101

COPY /target/myproject-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/myproject.war

EXPOSE 8080

USER root

WORKDIR /usr/local/tomcat/webapps 

CMD ["catalina.sh", "run"]
